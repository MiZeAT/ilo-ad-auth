# getPDfirmware

Set HPE ILO4/5 for Active Directory Authentication

# Prerequisites

    * HPEiLOCmdletsfrom https://www.powershellgallery.com/packages/HPEiLOCmdlets/
      Install-Module -Name HPEiLOCmdlets
    * MS ADS with ldaps
      check with command ldp.exe -> Connect ... --> domain.local (Port 636, SSL checked) 

# Filf iLOInput.csv 
- fill iLOInput.csv with ILO$ oder ILO% name/IPs,user,password

# How to use
1) Adopt Script Variables
 
-  $MyADS      = "domain.local"
- $MyUsers    = "OU=Users,OU=MyOU,DC=domain,DC=local" 
- $MyADSGroup = "CN=ILOAdmins,OU=Users,OU=MyOU,DC=domain,DC=local"


2) Execute
'.\ILO-AD-Auth.ps1'

