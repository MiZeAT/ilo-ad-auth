# Set HPE ILO4/5 for Active Directory Authentication

# Prerequisites 
# ldaps over Port 636
# test with command "ldp"


#import ILO Data
$myilos=Import-Csv -Path ./iLOInput.csv

# DS
$MyADS      = "domain.local"
$MyUsers    = "OU=Users,OU=MyOU,DC=domain,DC=local" 
$MyADSGroup = "CN=ILOAdmins,OU=Users,OU=MyOU,DC=domain,DC=local"


#go
foreach ($ilo in $myilos) {
    Write-Host "Connecting to ilo" $ilo.IP -ForegroundColor Green
    $connection = Connect-HPEiLO -IP $ilo.IP -Username $ilo.Username -Password $ilo.Password -DisableCertificateAuthentication
    if ($connection.iLOGeneration -eq "iLO4") {
        Write-Host "Set AD Directory Settings" -ForegroundColor Green
        $MyUindex = ,@(1)
        $MyUvalue = ,@($MyUsers)
        Set-HPEiLODirectorySetting -Connection $connection -LDAPDirectoryAuthentication DirectoryDefaultSchema -LocalUserAccountEnabled Yes -DirectoryServerAddress $MyADS -DirectoryServerPort 636 -UserContextIndex $MyUindex -UserContext $MyUvalue -Verbose

        Write-Host "Add AD Directory Group" -ForegroundColor Green
        Add-HPEiLODirectoryGroup -Connection $connection -GroupName $MyADSGroup -UserConfigPrivilege Yes -iLOConfigPrivilege Yes -RemoteConsolePrivilege Yes -VirtualMediaPrivilege Yes -VirtualPowerAndResetPrivilege Yes -LoginPrivilege Yes
    }
    if ($connection.iLOGeneration -eq "iLO5") {
        #modify syntax for Set-HPEiLODirectorySetting and Add-HPEiLODirectoryGroup to ILO5-style 
    }
    
    Write-Host "Disconnecting" $ilo.Ip -ForegroundColor Green
    Disconnect-HPEiLO -Connection $Connection
    
}
